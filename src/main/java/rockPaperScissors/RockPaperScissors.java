package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        
        
        
       

        

        while(true){

            
            
            


            System.out.println("Let's play round " + roundCounter  );
            System.out.println("Your choice (Rock/Paper/Scissors)?");
            String HumanMove = sc.next();

        
            

            //Verify human move / Human move print 
            if(!rpsChoices.contains(HumanMove)){
                System.out.println("I do not understand " + HumanMove + " Could you try again?");
            }
                else {
                //Computer random move:
            
                int random = (int)(Math.random() * 3);
                String ComputerMove = "";
            
                if(random == 0){
                    ComputerMove = "rock";
                } else if(random == 1){
                        ComputerMove = "paper";
                }   else {
                    ComputerMove = "scissors";
                }

                

                // Check win/loss/tie
                if(HumanMove.equals(ComputerMove)){
                    System.out.println("Human chose " + HumanMove + ", computer chose " + ComputerMove + ". It's a tie!" );
                    
                    System.out.println("Score: human " + humanScore + ", computer " + computerScore);
                    roundCounter = roundCounter+1;

                }else if ((HumanMove.equals("rock") && ComputerMove.equals("scissors")) || (HumanMove.equals("paper") && ComputerMove.equals("rock") || HumanMove.equals("scissors"))){
                    humanScore = humanScore+1; 
                    System.out.println("Human chose " + HumanMove + ", computer chose " + ComputerMove +  ". Human wins!" );
                  
                    System.out.println("Score: human " + humanScore + ", computer " + computerScore);
                    roundCounter = roundCounter+1;
                }
                else{
                    System.out.println("Human chose " + HumanMove + ", computer chose " + ComputerMove +  ". Computer wins" );
                    computerScore = computerScore+1;
                    
                    System.out.println("Score: human " + humanScore + ", computer " + computerScore);
                    roundCounter = roundCounter+1;
                }

                    

        }
        System.out.println("Do you wish to continue playing? (y/n)?");
            
            String KeepPlaying = sc.next();
            if (KeepPlaying.equals("n")){
                System.out.println("Bye bye :)");
                break;
            }

        
    }
    

          


}








    

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String HumanMove = sc.next();
        return HumanMove;
    }

}
